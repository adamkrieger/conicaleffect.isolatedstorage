﻿using System.IO.IsolatedStorage;

namespace ConicalEffect.IsolatedStorage
{
	public class IsoStorInterface
	{
		public IsolatedStorageFile UserStore
		{
			get { return IsolatedStorageFile.GetUserStoreForApplication(); }
		}
	}
}
